package org.nativescript.peche.httpserver;

import java.io.IOException;

import fi.iki.elonen.NanoHTTPD;

public class Runner extends NanoHTTPD {

    public String TAG_UUID = "#TAG#";
    public boolean SERVER_IS_UP =  false;

    public Runner(){
        super(9005);
        try {
            start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        } catch (IOException e) {
            this.SERVER_IS_UP = false;
        }
        this.SERVER_IS_UP = true;
    }

    public void restartHttp(){
        this.SERVER_IS_UP = false;
        stop();
        try {
            start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
            this.SERVER_IS_UP = true;
        } catch (IOException e) {
            this.SERVER_IS_UP = false;
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        String data = this.TAG_UUID;
        return newFixedLengthResponse(Response.Status.OK, "text/plain", data);
    }
}
